<?php namespace Dorigo;

class WP_Title extends \ArrayIterator {

    public function set(string $type, $content = null) {
        $this->offsetSet($type, $content);
    }

    public function get(string $type, $default = null) {
        return $this->offsetExists($type) ? $this->offsetGet($type) : $default;
    }

    public function __set(string $key, $value = null) {
        $this->set($key, $value);
    }

    public function __get(string $key) {
        return $this->get($key);
    }

}
