<?php namespace Dorigo;

use Dorigo\WP_Title;

class Titles {
    private static $filters;
    private $object;
    private $titles;

    private function addFilters() {
        add_filter("Dorigo/Titles", [$this, 'defaultImage'], 10, 2);
        self::$filters = true;
    }

    public function __construct($post = null) {

        global $wp_query;

        if(!$post) {
            if($wp_query->is_404()) {

                return $this->notFound();

            } else if($wp_query->is_search()) {

                return $this->search();

            } else if ($wp_query->is_date()) {

                return $this->date();

            }
        }

        if(is_null(self::$filters)) {
            $this->addFilters();
        }

        $this->object = $post ?: get_queried_object();
        $this->object = !is_object($this->object) ? get_post($post) : $this->object;

        switch(get_class($this->object)) {
            case "WP_Term":
                $this->taxonomy();
                break;
            case "WP_User":
                $this->user();
                break;
            case "WP_Post_Type":
                $this->postType();
                break;
            case "WP_Post":
                $this->post();
                break;
        }
    }

    private function postType() {

        $type = $this->object->name;
        $labels = $this->object->labels;

        $titles = new WP_Title();

        if($archive = get_field("page_for_{$type}","options")) {

            $titles = new self($archive);
            $titles = $titles->get();

        } else {

            $titles->title   = get_field("head_override_content_{$type}", "options") ? get_field("head_title_{$type}",   "options") : $labels->name;
            $titles->content = get_field("head_override_content_{$type}", "options") ? get_field("head_content_{$type}", "options") : null;
            $titles->image   = get_field("head_image_{$type}", 'options', 'array') ?: null;

        }

        $titles = apply_filters("Dorigo/Titles", $titles, $this->object);
        $titles = apply_filters("Dorigo/Titles/PostType", $titles, $this->object);

        $this->titles = apply_filters("Dorigo/Titles/PostType/post_type={$type}", $titles, $this->object);

        unset($titles);

    }

    private function post() {

        $titles = new WP_Title();

        $titles->title   = get_field('head_override_content', $this->object->ID) ? get_field('head_title', $this->object->ID) : get_the_title($this->object->ID);
        $titles->content = get_field('head_override_content', $this->object->ID) ? get_field('head_content', $this->object->ID) : null;
        $titles->image   = get_field('head_image', $this->object->ID, 'array') ?: null;

        $titles = apply_filters('Dorigo/Titles', $titles, $this->object);
        $titles = apply_filters('Dorigo/Titles/Post', $titles, $this->object);

        $this->titles = apply_filters('Dorigo/Titles/Post/post_type='.$this->object->post_type, $titles, $this->object);

        unset($titles);

    }

    private function taxonomy() {

        $titles = new WP_Title();

        $titles->title   = get_field('head_override_content', $this->object) ? get_field('head_title', $this->object) : $this->object->name;
        $titles->content = get_field('head_override_content', $this->object) ? get_field('head_content', $this->object) : wpautop($this->object->description);
        $titles->image   = get_field('head_image', $this->object, 'array') ?: null;


        $titles = apply_filters('Dorigo/Titles', $titles, $this->object);
        $this->titles = apply_filters('Dorigo/Titles/Taxonomy', $titles, $this->object);

        unset($titles);
    }

    private function user() {

        $user = $this->object;

        $titles = new WP_Title();
        $titles->title = "Posts by {$user->display_name}";

        $titles = apply_filters('Dorigo/Titles', $titles, $user);
        $this->titles = apply_filters('Dorigo/Titles/User', $titles, $user);

        unset($titles);
    }

    private function notFound() {

        $titles = new WP_Title();

        $titles->title   = get_field('404_head_override_content', 'options') ? get_field('404_head_title', 'options') : 'Page not found';
        $titles->content = get_field('404_head_override_content', 'options') ? get_field('404_head_content', 'options') : null;
        $titles->image   = get_field('404_head_image', 'options', 'array') ?: null;


        $this->titles = apply_filters('Dorigo/Titles', $titles, null);

        unset($titles);

    }

    private function search() {
        global $wp_query;

        $titles = new WP_Title();

        $titles->title   = 'You searched for “'.get_search_query().'”';
        $titles->content = $wp_query->found_posts. ' '. _n('Result', 'Results', $wp_query->found_posts);

        $this->titles = apply_filters('Dorigo/Titles', $titles, null);
        $this->titles = apply_filters('Dorigo/Titles/Search', $this->titles, null);

        unset($titles);
    }

    private function date() {
        global $wp_query;

        $titles = new self(get_option("page_for_posts"));

        $this->titles = apply_filters('Dorigo/Titles/Date', $titles->get(), null);

        unset($titles);
    }

    public function defaultImage($titles, $object) {
        if(is_null($titles->image)) {
            $titles->image = get_field('head_image','options','array');
        }

        if(!is_null($titles->image) && !is_array($titles->image)) {
            $titles->image = acf_get_attachment();
        }

        return $titles;
    }

    public function get($type = null) {
        if(is_a($this->titles, '\\Dorigo\\WP_Title')) {
            
            return !is_null($type) ? $this->titles->get($type) : $this->titles;
            
        } else {
            
            return null;
            
        }
    }
}
