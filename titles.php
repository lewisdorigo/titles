<?php
/**
 * Plugin Name:       Titles
 * Plugin URI:        https://bitbucket.org/madebrave/file
 * Description:       Simple WordPress class for getting the page title.
 * Version:           1.2.0
 * Author:            Lewis Dorigo
 * Author URI:        https://dorigo.co/
 */


if ( !defined( 'ABSPATH' ) ) {
	die();
}

if(function_exists("get_field")) {
    require_once __DIR__.'/classes/WP_Title.php';
    require_once __DIR__.'/lib/Titles.php';
} else {
    add_action('admin_notices', function() {
    	$class = 'notice notice-error';
    	$message = __('The Titles plugin requires Advanced Custom Fields to be enabled.');

    	printf('<div class="%1$s"><p>%2$s</p></div>', esc_attr($class), esc_html($message));
    });
}